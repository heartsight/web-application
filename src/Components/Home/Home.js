import React from "react";
import './Home.css';
import {Link} from "react-router-dom";

function Home() {
    return (
        <div id="home-section">
            <div className="main-home-container">
                <div className="welcome">
                    <div className="welcome-content">
                        <h1>Next-Gen Heart Rate Monitoring</h1>
                        <p>Accurate pulse data tracking and analysis for medical professionals.</p>
                        <button className="join-now-btn">
                            <Link to="/create-account">
                                <span className="button-text">Join Now</span>
                                <span className="arrow">&rarr;</span>
                            </Link>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Home;
