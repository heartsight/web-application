import React from 'react';
import './Contact.css'

function Contact() {
  return (
      <div className="contact-container-main">
        <div className="contact-content">
          <div className="contact-left-side">
            <div className="contact-address-details">
              <i className="fas fa-map-marker-alt"></i>
              <div className="contact-topic">Address</div>
              <div className="contact-text-one">777 Glades Rd
                <div className="contact-text-two">Boca Raton, FL 33431</div>
              </div>
            </div>
            <div className="contact-phone-details">
              <i className="fas fa-phone-alt"></i>
              <div className="contact-topic">Phone</div>
              <div className="contact-text-one">FAU: 561.297.3000</div>
            </div>
            <div className="contact-email-details">
              <i className="fas fa-envelope"></i>
              <div className="contact-topic">Email</div>
              <div className="contact-text-one">cclark36@fau.edu</div>
              <div className="contact-text-two">ecurtis2019@fau.edu</div>
              <div className="contact-text-two">jperrin2017@fau.edu</div>
              <div className="contact-text-two">msevryukova2017@fau.edu</div>
              <div className="contact-text-two">welcha2021@fau.edu</div>
            </div>
          </div>
          <div className="contact-right-side">
            <div className="contact-topic-text">Send us a message</div>
            <form action="#">
              <div className="contact-input-box">
                <input type="text" placeholder="Enter your name"/>
              </div>
              <div className="contact-input-box">
                <input type="text" placeholder="Enter your email"/>
              </div>
              <div className="contact-input-box contact-message-box">
                <input type="text"/>
              </div>
              <div className="contact-button">
                <input type="button" value="Send Now" />
              </div>
            </form>
          </div>
        </div>
      </div>
  );
}

export default Contact;
