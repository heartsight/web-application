import { Line } from "react-chartjs-2";
import "./LineChart.css";
import { useState, useEffect } from "react";
import {Chart as ChartJS, CategoryScale, LinearScale, PointElement, LineElement, Title, Tooltip, Legend, Filler} from 'chart.js';
import { getGradientLine, getGradientBackground } from "../../Services/ChartService"

ChartJS.register(CategoryScale, LinearScale, PointElement, LineElement, Title, Tooltip, Legend, Filler);

function LineChart({ pulseData }) {
    const[minValue, setMinValue] = useState(0);
    const[maxValue, setMaxValue] = useState(0);

    const [chartData, setChartData] = useState({
        labels: [],
        datasets: [{
            label: "BPM",
            data: [],
            borderWidth: 3.5,
            fill: true,
        }]
    });

    useEffect(() => {
        if (pulseData) {
            const sortedPulseData = pulseData.slice().sort((a, b) => a.pulseId - b.pulseId);

            const normalColor = "rgb(42,45,62)";
            const anomalyColor = "rgb(220,99,141)";
            const colors = sortedPulseData.map(pulse => pulse.anomaly ? anomalyColor : normalColor);
            const radius = sortedPulseData.map(pulse => pulse.anomaly ? 5 : 4);

            const minPulseValue = Math.min(...sortedPulseData.map(pulse => pulse.pulseValue)) - 20;
            const maxPulseValue = Math.max(...sortedPulseData.map(pulse => pulse.pulseValue)) + 20;

            setMinValue(minPulseValue);
            setMaxValue(maxPulseValue);

            setChartData({
                labels: sortedPulseData.map(pulse => {
                    const date = new Date(pulse.timestamp);
                    return `${date.getUTCMonth() + 1}/${date.getUTCDate() - 1}/${date.getUTCFullYear()}`
                }),
                datasets: [{
                    data: sortedPulseData.map(pulse => pulse.pulseValue),
                    borderColor: (context) => getGradientLine(context),
                    backgroundColor: (context) => getGradientBackground(context),
                    pointBackgroundColor: colors,
                    pointRadius: radius,
                    borderWidth: 2.5,
                    fill: true,
                }]
            });
        }
    }, [pulseData]);


    const options = {
        tension: 0.35,
        scales: {
            y: {
                display: true,
                beginAtZero: true,
                min: minValue,
                max: maxValue,
                ticks: {
                    stepSize: (maxValue - minValue) / 10,
                    maxTicksLimit: 10,
                },
            },
        },
        responsive: true,
        maintainAspectRatio: false,
        legend: {
            fontSize: 26,
        },
        plugins: {
            legend: {
                display: false
            },
            tooltip: {
                enabled: true,
                callbacks: {
                    title: function () {
                        return 'Heart Rate';
                    },
                    label: function (tooltipItem) {
                        if (tooltipItem.dataset && tooltipItem.dataset.data && tooltipItem.dataset.data.length > 0) {
                            const index = tooltipItem.dataIndex;
                            const pulse = pulseData[index]; // Extract the pulse object
                            const { pulseValue, anomaly, deviceType, timestamp } = pulse;

                            const time = new Date(timestamp)
                            const anomalyText = anomaly ? 'Yes' : 'No'

                            // Prepare AM/PM format
                            let hours = time.getUTCHours();
                            let minutes = time.getUTCMinutes();
                            let seconds = time.getUTCSeconds();
                            const ampm = hours >= 12 ? 'PM' : 'AM';

                            hours = hours % 12;
                            hours = hours ? hours : 12;
                            minutes = minutes < 10 ? '0' + minutes : minutes;
                            seconds = seconds < 10 ? '0' + seconds : seconds;
                            const strTime = `${hours}:${minutes}:${seconds} ${ampm}`;

                            return [
                                `BPM: ${pulseValue}`,
                                `Date: ${time.getMonth() + 1} / ${time.getDate()} / ${time.getFullYear()}`,
                                `Time: ${strTime}`,
                                `Anomaly: ${anomalyText}`,
                                `Device Type: ${deviceType}`
                            ];
                        }
                        return "";
                    }
                }
            },
        },
    };

    return (
        <div className="chart-container">
            <Line data={chartData}
            options={options}
             />
        </div>
    );
}

export default LineChart;
