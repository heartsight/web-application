import React from 'react';
import './AboutUs.css';

function AboutUs() {
    return (
        <div className="main-about-container">
            <div className="about-container">
                <div className="about-content">
                    <h2>About Us</h2>
                    <div className="content-containerR">
                        <div className="image-container">
                            <img src='https://media.discordapp.net/attachments/388984743527448576/1136497645771296828/HV1.png?width=605&height=605'  alt={"hope"}/>
                        </div>
                        <div className="text-container">
                            <h3>How does it work?</h3>
                            <p>
                                Our process is simple and user-friendly. To set up your Heart Vision camera, first plug in
                                the device and connect to your local internet.  After creating an account here on our website,
                                you can easily generate a personalized QR code that is linked to your profile. Place this
                                QR code in view of any Heart Vision camera, and within seconds, our advanced rPPG algorithms
                                will analyze the subtle changes in skin color caused by blood flow to accurately measure
                                your heart rate.
                            </p>
                        </div>
                    </div>

                    <div className="content-containerL">
                        <div className="text-container">
                            <h3>Tracking Made Effortless</h3>
                            <p>
                                Gone are the days of cumbersome heart rate monitoring devices. With Heart Vision you can
                                effortlessly track your heart rate over time without any additional hardware. Whether
                                you're at home, in the office, or on the go, simply scan your QR code using your device's
                                camera, and we'll take care of the rest.
                            </p>
                        </div>
                        <div className="image-container">
                            <img src='https://media.discordapp.net/attachments/388984743527448576/1136497646140407859/HV2.png?width=605&height=605'  alt={"heart"}/>
                        </div>
                    </div>

                    <div className="content-containerR">
                        <div className="image-container">
                            <img src='https://media.discordapp.net/attachments/388984743527448576/1136497646580813945/HV3.png?width=605&height=605'  alt={"data"}/>
                        </div>
                        <div className="text-container">
                            <h3>Insightful Heart Rate Data</h3>
                            <p>
                                Our platform not only provides real-time heart rate measurements but also compiles and
                                presents your heart rate data in easy-to-read graphs and charts. This valuable insight
                                allows you to observe trends, identify patterns, and gain a deeper understanding of how
                                your heart rate responds to different activities and situations.
                            </p>
                        </div>
                    </div>

                    <div className="content-containerL">
                        <div className="text-container">
                            <h3>Privacy and Security</h3>
                            <p>
                                We understand the importance of privacy and data security. Rest assured that all your
                                heart rate data is encrypted and stored securely on our servers. In fact, our device
                                never sends identifiable footage over the cloud whatsoever and only extracts the minimum
                                necessary color values while deleting the rest of the recording locally.  We comply with
                                strict privacy standards, and your information will never be shared with third parties
                                without your consent. Our company is committed to ensuring that individuals have complete
                                control over their data and that they are aware of how their data is being used in order
                                to protect sensitive health information, build trust in healthcare technology, and uphold
                                the highest ethical standards.
                            </p>
                        </div>
                        <div className="image-container">
                            <img src='https://media.discordapp.net/attachments/388984743527448576/1136497647436435486/HV4.png?width=530&height=518'  alt={"face"}/>
                        </div>
                    </div>

                    <div className="content-containerR">
                        <div className="image-container">
                            <img src='https://media.discordapp.net/attachments/388984743527448576/1136497647776186479/HV5.png?width=605&height=605'  alt={"family"}/>
                        </div>
                        <div className="text-container">
                            <h3>Our Mission</h3>
                            <p>
                                Our mission is to empower individuals with the knowledge and tools to take charge of their
                                heart health with your personal privacy at the forefront. By making heart rate tracking
                                accessible and convenient, we aim to encourage everyone to prioritize their well-being and
                                make informed lifestyle choices.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default AboutUs;

