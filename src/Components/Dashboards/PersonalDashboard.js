import React, {useState} from 'react';
import { AnomalyCard, CountCard, UpdatedCard, AverageCard } from './Components/SummaryCard/SummaryCard';
import LineChart from '../Charts/LineChart';
import PulseTable from "./Components/Tables/PulseTable";
import CustomDatePicker from "./Components/DatePicker/DatePicker"
import TimeRangeButtons from "./Components/DatePicker/TimeRangeButtons";
import "./Dashboard.css"
import {FaDownload, FaShareAlt} from "react-icons/fa";
import {downloadPulse} from "../../Services/FileService";
import SharePulse from "./Components/Share/SharePulse";

const PersonalDashboard = ({userData, pulseData, dateRange, setDateRange}) => {
    const [isEmailModalOpen, setEmailModalOpen] = useState(false);
    const handleDownload = async () => {
        await downloadPulse(userData.userId);
    };

    const handleShare = () => {
        setEmailModalOpen(true);
    };

    const handleCloseModal = () => {
        setEmailModalOpen(false);
    };

    return (
        <div>
            <div className="Dash-chart">
                <div className="chart-header">
                    <h2>BPM Trends</h2>
                    <div className="chart-controls">
                        <button onClick={handleDownload}><FaDownload /></button>
                        <button onClick={handleShare}><FaShareAlt /></button>
                    </div>
                </div>
                <div className="time-range-container">
                    <TimeRangeButtons setDateRange={setDateRange} />
                    <div className="date-picker">
                        <CustomDatePicker
                            startDate={dateRange.start}
                            endDate={dateRange.end}
                            onDateChange={(name, value) => setDateRange({ ...dateRange, [name]: value })}
                        />
                    </div>
                </div>
                {pulseData.length > 0 ? (
                    <LineChart pulseData={pulseData} />
                ) : (
                    <div className="no-data-message">There is currently no heart rate data.</div>
                )}
            </div>

            <div className="Dash-summary">
                <div className="summary">
                    <CountCard pulseCount={userData ? userData.pulseCount : 0} />
                </div>
                <div className="summary">
                    <AverageCard pulseAvg={userData ? userData.pulseAverage : 0} />
                </div>
                <div className="summary">
                    <UpdatedCard updated={userData ? userData.updated : {}} />
                </div>
                <div className="summary">
                    <AnomalyCard pulseData={pulseData ? pulseData : {}} />
                </div>
            </div>

            <div className="Dash-data">
                <PulseTable pulseData={pulseData} />
            </div>
            <SharePulse
                isOpen={isEmailModalOpen}
                onRequestClose={handleCloseModal}
                userId={userData.userId}
            />
        </div>
    )
}

export default PersonalDashboard;
