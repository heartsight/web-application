import React, { useEffect, useState } from 'react';
import { auth } from '../../Configuration/Firebase';
import { subMonths} from 'date-fns';
import { getUserData } from '../../Services/UserService';
import { getRoles } from '../../Services/UserService';
import { useNavigate } from "react-router-dom";
import HouseholdDashboard from "./HouseholdDashboard";
import PersonalDashboard from "./PersonalDashboard";
import 'firebase/auth';
import "./Dashboard.css"
import QRCodeModal from "./Components/QrCode/QrCode";
import HouseholdListModal from "./Components/HouseholdList/HouseholdListModal"
import { FaQrcode } from 'react-icons/fa';
import { FaHome } from 'react-icons/fa';
import { removeUserFromHousehold } from '../../Services/UserService';

function Dashboard() {
    const [showQRModal, setShowQRModal] = useState(false);
    const [userData, setUserData] = useState({});
    const [pulseData, setPulseData] = useState([]);
    const [households, setHouseholds] = useState([]);
    const [dateRange, setDateRange] = useState({ start: subMonths(new Date(), 1), end: new Date() });
    const [selected, setSelected] = useState("Personal");
    const [showHouseholdListModal, setShowHouseholdListModal] = useState(false);

    useNavigate();

    const fetchRoleData = async () => {
        const currentUser = auth.currentUser;
        if (currentUser) {
            const fetchedRoleData = await getRoles(currentUser.uid);
            const households = [
                ...fetchedRoleData.memberOf.map(household => ({ ...household, role: 'Member' })),
                ...fetchedRoleData.viewerOf.map(household => ({ ...household, role: 'Viewer' })),
                ...fetchedRoleData.moderatorOf.map(household => ({ ...household, role: 'Moderator' })),
                ...fetchedRoleData.adminOf.map(household => ({ ...household, role: 'Admin' })),
                ...fetchedRoleData.ownerOf.map(household => ({ ...household, role: 'Owner' }))
            ];
            setHouseholds(households);
        }
    };

    const handleUserLeftHousehold = async () => {
        fetchRoleData().catch((error) => {
            console.error('Error fetching role data:', error);
        });
    };

    useEffect(() => {
        const fetchUserData = async () => {
            const currentUser = auth.currentUser;
            const start = dateRange.start
            const end = dateRange.end
            const startDate = `${start.getUTCFullYear()}-${start.getUTCMonth() + 1}-${start.getUTCDate()}T00:00:00.0000000Z`
            const endDate = `${end.getUTCFullYear()}-${end.getUTCMonth() + 1}-${end.getUTCDate()}T23:59:59.9999999Z`

            if (currentUser) {
                const fetchedUserData = await getUserData(currentUser.uid, startDate, endDate);
                if (fetchedUserData.pulse) {
                    setPulseData(fetchedUserData.pulse);
                    delete fetchedUserData.pulse;
                }
                setUserData(fetchedUserData);
            }
        };

        fetchUserData().catch((error) => {
            console.error('Error fetching user data:', error);
        });

        fetchRoleData().catch((error) => {
            console.error('Error fetching role data:', error);
        });

    }, [dateRange.end, dateRange.start]);

    const toggleQRModal = () => {
        setShowQRModal(true);
    }

    const toggleHouseholdListModal = () => {
        setShowHouseholdListModal(true);
    }

    return (
        <div className="Dash">
            <div className="Dash-main">
                <header className="Dash-header">
                    <div className="Introduction">
                        <h1>Dashboard</h1>
                        {userData && userData.firstName ? `Welcome, ${userData.firstName}` : 'Loading...'}
                    </div>
                    <div className="button-container">
                        <button className={`household-list-button ${showHouseholdListModal ? 'selected' : ''}`} onClick={toggleHouseholdListModal}>
                            <FaHome />
                        </button>
                        <button className={`qr-button ${showQRModal ? 'selected' : ''}`} onClick={toggleQRModal}>
                            <FaQrcode />
                        </button>
                        <div className="personal-household-container">
                            <button className={`left-button ${selected === 'Personal' ? 'selected' : ''}`} onClick={() => setSelected('Personal')}>Personal</button>
                            <button className={`right-button ${selected === 'Household' ? 'selected' : ''}`} onClick={() => setSelected('Household')}>Household</button>
                        </div>
                    </div>
                </header>
                <HouseholdListModal
                    isOpen={showHouseholdListModal}
                    onRequestClose={() => setShowHouseholdListModal(false)}
                    households={households}
                    userId={auth.currentUser ? auth.currentUser.uid : ''}
                    onLeaveHousehold={removeUserFromHousehold}
                    onUserLeftHousehold={handleUserLeftHousehold} // Pass the callback
                    onCreateHousehold={fetchRoleData} // Pass the function to fetch household data
                />

                <QRCodeModal
                    isOpen={showQRModal}
                    onRequestClose={() => setShowQRModal(false)}
                    userId={auth.currentUser ? auth.currentUser.uid : ''}
                />
                {selected === 'Personal' ? (
                    <PersonalDashboard
                        userData={userData}
                        pulseData={pulseData}
                        dateRange={dateRange}
                        setDateRange={setDateRange}
                    />
                ) : (
                    <HouseholdDashboard
                        households={households}
                        userId={auth.currentUser ? auth.currentUser.uid : ''}
                    />
                )}
            </div>
        </div>
    );
}

export default Dashboard;