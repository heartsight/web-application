import React, {useState} from 'react';
import "./Manage.css"
import ManageModal from './Modal/ManageModal';

function Manage ({ users, onSelect, currentUserRole, householdId, setNeedRender, userRoleLevel }) {
    const [isManageModalOpen, setIsManageModalOpen] = useState(false);

    return (
        <div className="manage">
            <ManageModal
                isOpen={isManageModalOpen}
                onRequestClose={() => setIsManageModalOpen(false)}
                users={users}
                onSelect={onSelect}
                curentUserRole={currentUserRole}
                householdId={householdId}
                setNeedRender={setNeedRender}
                userRoleLevel={userRoleLevel}
            />
            <button className="manage-button" onClick={() => setIsManageModalOpen(true)}>Manage</button>
        </div>
    );
};

export default Manage;
