import React, {useState} from 'react';
import './ManageModal.css';
import UsersTab from "./Tabs/UsersTab"
import PendingInvitesTab from "./Tabs/PendingInvitesTab"
import InvitesTab from "./Tabs/InvitesTab"
import {
    modifyNotificationMode,
    modifyPendingInvites,
    modifyUserNickname, removeInvites, removeUsers, sendInvite
} from "../../../../../../Services/ManageService";
import { modifyUserRole } from "../../../../../../Services/ManageService";
import { verifyEmail } from "../../../../../../Services/UserService";
import {auth} from "../../../../../../Configuration/Firebase";

function ManageModal({ isOpen, onRequestClose, users, householdId, setNeedRender, userRoleLevel }) {
    const [activeTab, setActiveTab] = useState('users');
    const [nicknameChanges, setNicknameChanges] = useState([]);
    const [roleChanges, setRoleChanges] = useState([]);
    const [timeChanges, setTimeChanges] = useState([]);
    const [inviteRoleChanges, setInviteRoleChanges] = useState([]);
    const [inviteChanges, setInviteChanges] = useState([]);
    const [userChanges, setUserChanges] = useState([]);
    const [email, setEmail] = useState('');
    const [nickname, setNickname] = useState('');
    const [role, setRole] = useState('');
    const [errorMessage, setErrorMessage] = useState('');

    const checkUserRoleLevel = () => {
        if (userRoleLevel < 3) {
            setErrorMessage('Error: You do not have the necessary permissions to make changes');
            return false;
        }
        return true;
    };

    const handleSaveChanges = async () => {
        let needsRender = false;
        let invalidChanges = [];

        if (!checkUserRoleLevel()) return false;

        if (roleChanges && roleChanges.length > 0) {
            for (let i = 0; i < roleChanges.length; i++) {
                const change = roleChanges[i];

                console.log("roleChanges:", roleChanges)

                if (userRoleLevel < 4 && (change.newRole === 'Moderator' || change.newRole === 'Admin' || change.newRole === 'Owner')) {
                    invalidChanges.push(change);
                    continue;
                }
                if (userRoleLevel < 5 && (change.newRole === 'Admin' || change.newRole === 'Owner')) {
                    invalidChanges.push(change);
                    continue;
                }

                let response = await modifyUserRole([change], householdId);
                if (response.success) {
                    needsRender = true;
                }
            }
        }

        if (invalidChanges.length > 0) {
            let errorMessages = invalidChanges.map(change =>
                `Error: You do not have permission to modify user ${change.userId} to ${change.newRole}`
            ).join('\n');
            setErrorMessage(errorMessages);
            return false;
        }

        if (nicknameChanges && nicknameChanges.length > 0) {
            let response = await modifyUserNickname(nicknameChanges, householdId);
            if (response.success) {
                setNicknameChanges([]);
                needsRender = true;
            }
        }

        if (timeChanges && timeChanges.length > 0) {
            let response = await modifyNotificationMode(timeChanges, householdId);
            if (response.success) {
                setTimeChanges([]);
                needsRender = true;
            }
        }

        if (inviteRoleChanges && inviteRoleChanges.length > 0) {
            let response = await modifyPendingInvites(inviteRoleChanges, householdId);
            if (response.success) {
                setInviteRoleChanges([]);
                needsRender = true;
            }
        }

        if (inviteChanges && inviteChanges.length > 0) {
            let response = await removeInvites(inviteChanges, householdId);
            if (response.success) {
                setInviteChanges([]);
                needsRender = true;
            }
        }

        if (userChanges && userChanges.length > 0) {
            let response = await removeUsers(userChanges, householdId);
            if (response.success) {
                setUserChanges([]);
                needsRender = true;
            }
        }

        if (needsRender) {
            setNeedRender(true);
            setErrorMessage('');
        }

        return needsRender;
    };

    const handleSent = async () => {
        const verifyResponse = await verifyEmail(email);
        if (verifyResponse === 'false') {
            console.error('Email verification failed', verifyResponse.error);
            return;
        }

        let needsRender = false;
        let response = await sendInvite(email, nickname, householdId, auth.currentUser.uid, role);
        if (response.success) {
            needsRender = true;
        }

        if (needsRender) {
            setNeedRender(true);
            setEmail('');
            setNickname('');
            setRole('');
        }
    };

    const addNickname = async (newChange) => {
        setNicknameChanges([...nicknameChanges, newChange])
    }

    const addRoleChange = (newChange) => {
        const existingChangeIndex = roleChanges.findIndex(change => change.userId === newChange.userId);

        if (existingChangeIndex !== -1) {
            const updatedRoleChanges = [...roleChanges];
            updatedRoleChanges[existingChangeIndex] = newChange;
            setRoleChanges(updatedRoleChanges);
        } else {
            setRoleChanges(prevRoleChanges => [...prevRoleChanges, newChange]);
        }
    };

    const addTimeChange = (newChange) => {
        setTimeChanges([...timeChanges, newChange]);
    };

    const addInviteRoleChange = (newChange) => {
        setInviteRoleChanges([...inviteRoleChanges, newChange]);
    };

    const addInviteChange = (newChange) => {
        setInviteChanges([...inviteChanges, newChange]);
    };

    const addUserChange = (newChange) => {
        setUserChanges([...userChanges, newChange]);
    };

    if (!isOpen) {
        return null;
    }

    const onRequestCloseModal = () => {
        onRequestClose();
        setErrorMessage('');
    };

    return (
        <div className="manage-modal">
            <div className="manage-modal-container">
                <div className="manage-close-container">
                    <span className="manage-close-button" onClick={onRequestClose}>&times;</span>
                </div>
                <div className="manage-modal-content">
                    <div className="manage-title">
                        <h2>Manage</h2>
                    </div>
                    <div className="manage-modal-navigation">
                        <button className={`tab-button ${activeTab === 'users' ? 'active' : ''}`} onClick={() => setActiveTab('users')}>Users</button>
                        <button className={`tab-button ${activeTab === 'pending-invites' ? 'active' : ''}`} onClick={() => setActiveTab('pending-invites')}>Pending Invites</button>
                        <button className={`tab-button ${activeTab === 'invites' ? 'active' : ''}`} onClick={() => setActiveTab('invites')}>Invite</button>
                    </div>
                    {activeTab === 'users' &&
                        <div className="main-contents">
                            <div className="tab-contents">
                                <UsersTab
                                    users={users}
                                    addNickname={addNickname}
                                    addRoleChange={addRoleChange}
                                    addTimeChange={addTimeChange}
                                    addUserChange={addUserChange}
                                    userRoleLevel={userRoleLevel}
                                />
                                {errorMessage &&
                                    <div className="users-error-message">
                                        <p>{errorMessage}</p>
                                    </div>
                                }
                                <div className="save-cancel-buttons">
                                    <div className="save-changes-button">
                                        <button onClick={async () => {
                                            if (await handleSaveChanges()) {
                                                onRequestCloseModal();
                                            }
                                        }}>Save Changes</button>
                                    </div>
                                    <div className="cancel-button">
                                        <button onClick={onRequestCloseModal}>Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    }
                    {activeTab === 'pending-invites' &&
                        <div className="main-contents">
                            <div className="tab-contents">
                                <PendingInvitesTab
                                    householdId={householdId}
                                    addInviteRoleChange={addInviteRoleChange}
                                    addInviteChange={addInviteChange}
                                    userRoleLevel={userRoleLevel}
                                />
                                <div className="save-cancel-buttons">
                                    <div className="save-changes-button">
                                        <button onClick={async () => {
                                            await handleSaveChanges();
                                            onRequestClose();
                                        }}>Save Changes</button>
                                    </div>
                                    <div className="cancel-button">
                                        <button onClick={onRequestClose}>Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    }
                    {activeTab === 'invites' &&
                        <div className="main-contents">
                            <div className="tab-contents">
                                <InvitesTab
                                    email={email}
                                    nickname={nickname}
                                    role={role}
                                    setEmail={setEmail}
                                    setRole={setRole}
                                    setNickname={setNickname}
                                    userRoleLevel={userRoleLevel}
                                />
                                <div className="sent-buttons">
                                    <div className="sent-button">
                                        <button onClick={async () => {
                                            await handleSent();
                                        }}>Send</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    }
                </div>
            </div>
        </div>
    );
}

export default ManageModal;
