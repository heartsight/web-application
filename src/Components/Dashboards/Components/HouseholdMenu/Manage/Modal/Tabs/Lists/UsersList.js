import { useEffect, useState } from 'react';
import { getUserEmail } from "../../../../../../../../Services/UserService";
import './UsersList.css'

function UsersList({ users, role, addNickname, addRoleChange, addTimeChange, addUserChange, isRemoved, handleRemove, userRoleLevel}) {
    const [editingUserId, setEditingUserId] = useState(null);
    const [nickname, setNickname] = useState('');

    const useEmail = (userId) => {
        const [email, setEmail] = useState('');
        useEffect(() => {
            const fetchEmail = async () => {
                const email = await getUserEmail(userId);
                setEmail(email);
            };
            fetchEmail();
        }, [userId]);
        return email;
    };

    const email = useEmail(users.userId);

    const handleRoleChange = (userId, newRole) => {
        let user = {
            userId: userId,
            newRole: newRole,
            oldRole: role
        }
        addRoleChange(user);
    };

    const handleNicknameChange = (userId, newNickname) => {
        let user = {
            userId: userId,
            role: role,
            newNickname: newNickname
        }
        addNickname(user);
    };

    const handleUserChange = (userId) => {
        let user = {
            userId: userId,
            role: role,
        }
        addUserChange(user);
    };

    const handleNicknameClick = (userId) => {
        setEditingUserId(userId);
        setNickname(users.userNickname);
    }

    const roleToLevel = (role) => {
        switch (role) {
            case "Viewer":
                return 1;
            case "Member":
                return 2;
            case "Moderator":
                return 3;
            case "Admin":
                return 4;
            case "Owner":
                return 5;
            default:
                return 0;
        }
    };

    // const handleNicknameBlur = () => {
    //     setEditingUserId(null);
    // };

    return (
        users ? (
            <div className={`user-item ${isRemoved ? 'removed' : ''}`}>
                <div className="nickname-email">
                    {editingUserId === users.userId ? (
                        <input
                            type="text"
                            value={nickname}
                            onChange={(e) => {
                                setNickname(e.target.value);
                                handleNicknameChange(users.userId, e.target.value);
                            }}
                            autoFocus
                        />
                    ) : (
                        <span onClick={() => handleNicknameClick(users.userId)}>{users.userNickname}</span>
                    )}
                    <p>{email}</p>
                </div>
                {userRoleLevel > roleToLevel(role) ? (
                    <div className="user-dropdown">
                        <select defaultValue={role} onChange={(e) => handleRoleChange(users.userId, e.target.value)}>
                            {userRoleLevel > roleToLevel("Viewer") && <option value="Viewer">Viewer</option>}
                            {userRoleLevel > roleToLevel("Member") && <option value="Member">Member</option>}
                            {userRoleLevel > roleToLevel("Moderator") && <option value="Moderator">Moderator</option>}
                            {userRoleLevel > roleToLevel("Admin") && <option value="Admin">Admin</option>}
                            {userRoleLevel > roleToLevel("Owner") && <option value="Owner">Owner</option>}
                        </select>
                    </div>
                ) : (
                    <div className="user-role-display">{role}</div>
                )}
                {userRoleLevel > roleToLevel(role) && (
                    <div className="user-close-button-container">
                        <button
                            className="user-close-button"
                            onClick={() => {
                                handleUserChange(users.userId);
                                handleRemove(users.userId);
                            }}
                        >
                            &times;
                        </button>
                    </div>
                )}
            </div>
        ) : null
    );
}

export default UsersList;
