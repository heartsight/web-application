import "./Tabs.css"
function InvitesTab({email, nickname, role, setEmail, setNickname, setRole, userRoleLevel}) {
    const roles = [
        { role: 'Member', level: 1 },
        { role: 'Viewer', level: 2 },
        { role: 'Moderator', level: 3 },
        { role: 'Admin', level: 4 },
        { role: 'Owner', level: 5 },
    ];

    const handleInputChange = (event, setter) => {
        setter(event.target.value);
    }

    const handleRoleChange = (event) => {
        setRole(event.target.value);
    }

    return (
        <div className="invites-tab-contents">
            <div className="invites-subtitle">
                <p>Sent Invite</p>
            </div>
            <div className="invites-inputs-container">
                <div className="invites-email-nickname-inputs">
                    <div className="invites-email-input">
                        <p>Email:</p>
                        <input
                            type="email"
                            value={email}
                            onChange={(event) => handleInputChange(event, setEmail)}
                            placeholder="Email"
                        />
                    </div>
                    <div className="invites-nickname-input">
                        <p>Nickname:</p>
                        <input
                            type="text"
                            value={nickname}
                            onChange={(event) => handleInputChange(event, setNickname)}
                            placeholder="Nickname"
                        />
                    </div>
                </div>
                <div className="invites-role-input">
                    <p>Role:</p>
                    {roles.filter(roleOption => roleOption.level < userRoleLevel).map((roleOption, index) => (
                        <div key={index} className="invites-role">
                            <input
                                type="radio"
                                id={roleOption.role}
                                name="role"
                                value={roleOption.role}
                                checked={role === roleOption.role}
                                onChange={handleRoleChange}
                            />
                            <label htmlFor={roleOption.role}>
                                <p>{roleOption.role}</p>
                            </label>
                        </div>
                    ))}
                </div>
            </div>
        </div>
    );
};

export default InvitesTab;