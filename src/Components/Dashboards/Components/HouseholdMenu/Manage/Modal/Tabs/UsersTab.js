import "./Tabs.css"
import {useState} from "react";
import UsersList from "./Lists/UsersList";

function UsersTab({ users, addNickname, addRoleChange, addTimeChange, addUserChange, userRoleLevel }) {
    const [removeUser, setRemoveUser] = useState([]);
    const [isOpen, setIsOpen] = useState({
        Member: true,
        Viewer: true,
        Moderator: true,
        Admin: true,
        Owner: true
    });

    const roles = ['Member', 'Viewer', 'Moderator', 'Admin', 'Owner'];

    const handleRemoveUser = (userId) => {
        setRemoveUser(prev => [...prev, userId]);
    };

    const handleToggleOpen = (role) => {
        setIsOpen(prev => ({
            ...prev,
            [role]: !prev[role],
        }));
    };

    return (
        <div className="users-tab-content">
            {roles.map(role => {
                const userList = users[role.toLowerCase() + 's'];
                return (
                    userList.length > 0 ? (
                        <div className="UserList">
                            <div key={role}>
                                <h3 onClick={() => { handleToggleOpen(role); }}>
                                    {role}s
                                    <span className={`arrow ${isOpen[role] ? 'up' : 'down'}`}>▼</span>
                                </h3>
                                {isOpen[role] && userList.map((user) => (
                                    <UsersList
                                        users={user}
                                        role={role}
                                        addNickname={addNickname}
                                        addRoleChange={addRoleChange}
                                        addTimeChange={addTimeChange}
                                        addUserChange={addUserChange}
                                        isRemoved={removeUser.includes(user.userId)}
                                        handleRemove={handleRemoveUser}
                                        userRoleLevel={userRoleLevel}
                                    />
                                ))}
                            </div>
                        </div>
                    ) : null
                );
            })}
        </div>
    );
}

export default UsersTab;
