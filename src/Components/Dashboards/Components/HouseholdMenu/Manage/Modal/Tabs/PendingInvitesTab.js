import "./Tabs.css"
import { retrieveNotifications } from "../../../../../../../Services/ManageService";
import React, { useEffect, useState} from "react";
import InviteNotificationHousehold from "../../../../../../Navigation/NotificationTypes/InviteNotificationHousehold";

function PendingInvitesTab({householdId, addInviteRoleChange, addInviteChange, userRoleLevel}) {
    const [notifications, setNotifications] = useState([]);
    const [needRender, setNeedRender] = useState(false);
    const [closedInvites, setClosedInvites] = useState([]);

    useEffect(() => {
        const fetchNotifications = async () => {
            let response = await retrieveNotifications(householdId);

            if (response.success) {
                setNotifications(response.invites);
            }
        };

        fetchNotifications();
        setNeedRender(false);
    }, [householdId, needRender]);

    const handleCloseInvite = (inviteId) => {
        setClosedInvites(prev => [...prev, inviteId]);
    };

    return (
        <div className="pending-invite-tab-content">
            <div className="pending-invite-notification">
                <div className="pending-invites-subtitles">
                    <div className="subtitles-text">Email</div>
                    <div className="subtitles-text">Date</div>
                    <div className="subtitles-text">Role</div>
                    <div className="subtitles-text"></div>
                </div>
                {notifications.map((invite, index) => (
                    <InviteNotificationHousehold
                        notification={invite}
                        addInviteRoleChange={addInviteRoleChange}
                        addInviteChange={addInviteChange}
                        isClosed={closedInvites.includes(invite.id)}
                        handleClose={handleCloseInvite}
                        userRoleLevel={userRoleLevel}
                    />
                ))}
            </div>
        </div>
    );
}

export default PendingInvitesTab;