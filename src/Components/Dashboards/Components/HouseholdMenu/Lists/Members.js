import './Members.css'

function Members({ users, onSelect }) {
    return (
        users && users.members && users.members.length > 0 ? (
            <div className="MemberList">
                <h3>Members</h3>
                {users.members.map((user, index) => (
                    <div key={index} className="member-item" onClick={() => onSelect(user.userId, 'Member')}>
                        <span>{user.userNickname}</span>
                    </div>
                ))}
            </div>
        ) : null
    );

}

export default Members;
