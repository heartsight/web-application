import React from 'react';
import './DeleteUserModal.css';

const DeleteUserModal = ({ isOpen, onRequestClose, onUserRemove, user }) => {
    const handleUserRemove = async () => {
        if (user) {
            const result = await onUserRemove(user.userId, user.householdId, user.currentRole);
            if (result.success) {
                onRequestClose();
            } else {
                console.error('Failed to remove user:', result.error);
            }
        } else {
            console.error('No user selected for removal');
        }
    };

    if (!isOpen) {
        return null;
    }

    return (
        <div className="delete-modal">
            <div className="delete-modal-content">
                <div className="delete-close-container">
                    <span className="manage-close-button" onClick={onRequestClose}>&times;</span>
                </div>
                <h2>Remove user</h2>
                <p>Are you sure you want to remove this user?</p>
                <div className="button-container">
                    <button className="yes-button" onClick={handleUserRemove}>Yes</button>
                    <button className="no-button" onClick={onRequestClose}>No</button>
                </div>
            </div>
        </div>
    );
};

export default DeleteUserModal;

