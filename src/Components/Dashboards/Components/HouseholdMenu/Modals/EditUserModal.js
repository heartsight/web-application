import React, {useEffect, useState} from 'react';
import {modifyUserRole} from "../../../../../Services/UserService";
import {modifyUserNickname} from "../../../../../Services/UserService";
import './EditUserModal.css';

const EditUserModal = ({ isOpen, onRequestClose, user, onRoleChange, onNicknameChange }) => {
    const [selectedRole, setSelectedRole] = useState(null);
    const [nickname, setNickname] = useState("");
    const [hasRoleChanged, setHasRoleChanged] = useState(false);
    const [hasNicknameChanged, setHasNicknameChanged] = useState(false);

    useEffect(() => {
        if (user) {
            setSelectedRole(user.currentRole);
            setNickname(user.userNickname);
            setHasRoleChanged(false);
            setHasNicknameChanged(false);
        }
    }, [user]);

    const handleRoleChange = (e) => {
        setSelectedRole(e.target.value);
        setHasRoleChanged(true);
    };

    const handleNicknameChange = (e) => {
        setNickname(e.target.value);
        setHasNicknameChanged(true);
    };

    const handleSaveChanges = async () => {
        let roleResult = { success: true };
        let nicknameResult = { success: true };

        if (hasRoleChanged) {
            roleResult = await modifyUserRole(user.userId, user.householdId, user.currentRole, selectedRole);
        }

        if (hasNicknameChanged) {
            nicknameResult = await modifyUserNickname(user.userId, user.householdId, user.currentRole, nickname);
        }

        if (roleResult.success && nicknameResult.success) {
            if (hasRoleChanged) {
                onRoleChange(user.userId, selectedRole);
            }
            if (hasNicknameChanged) {
                onNicknameChange(user.userId, nickname);
            }
            onRequestClose();
        } else {
            console.error(roleResult.error, nicknameResult.error);
        }
    };

    if (!isOpen || !user) {
        return null;
    }

    return (
        <div className="edit-modal">
            <div className="edit-modal-content">
                <div className="edit-close-container">
                    <span className="manage-close-button" onClick={onRequestClose}>&times;</span>
                </div>
                <h2>Edit user</h2>
                <p>
                    Nickname:
                    <input type="text" value={nickname} onChange={handleNicknameChange} />
                </p>
                <p>User Email: {user.email}</p>
                <p>
                    <label>
                        Role:
                        <select value={selectedRole} onChange={handleRoleChange}>
                            <option value="Member">Member</option>
                            <option value="Viewer">Viewer</option>
                            <option value="Admin">Admin</option>
                            <option value="Moderator">Moderator</option>
                            <option value="Owner">Owner</option>
                        </select>
                    </label>
                </p>
                <p>Member Since: {new Date(user.created).toLocaleDateString()}</p>
                <button className="save-changes-button" onClick={handleSaveChanges}>Save Changes</button>
            </div>
        </div>
    );
};

export default EditUserModal;
