import React from "react";
import './HouseholdSelect.css'

const HouseholdSelect = ({ households, handleSelectHousehold, selectedHousehold }) => {
    return (
        <select className="HouseholdSelect" onChange={handleSelectHousehold} value={selectedHousehold || ''}>
            <option value="">Select a household...</option>
            {Array.isArray(households) && households.map(household => (
                <option key={household.householdId} value={household.householdId}>
                    {household.householdName}
                </option>
            ))}
        </select>
    );
};

export default HouseholdSelect;
