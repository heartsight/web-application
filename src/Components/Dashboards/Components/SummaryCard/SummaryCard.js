import React from 'react';
import "./SummaryCard.css"

export function AnomalyCard({ pulseData }) {
    const anomalyCount = pulseData.filter(pulse => pulse.anomaly === true).length;

    const getImage = () => {
        if (anomalyCount === 0) return "https://cdn.discordapp.com/attachments/314406205315481603/1128214862523285544/check.png";
        if (anomalyCount > 0 && anomalyCount <= 3) return "https://cdn.discordapp.com/attachments/314406205315481603/1128215694069223454/warning.png";
        if (anomalyCount >= 4) return "https://cdn.discordapp.com/attachments/314406205315481603/1128215955726663720/bigwarn.png";
    }

    return (
        <div className="main">
            <img src={getImage()} alt="Heart" className="image" />
            <div className="text-content">
                <h3>{anomalyCount}</h3>
                <p>Recent Anomalies</p>
            </div>
        </div>
    );
}

export function AverageCard({ pulseAvg }) {
    const average = () => {
        if (typeof pulseAvg === 'number') {
            return pulseAvg.toFixed(1);
        } else {
            return 'N/A';
        }
    }

    return (
        <div className="main">
            <img src="https://cdn.discordapp.com/attachments/388984743527448576/1126292137609269319/Untitled-2.png" alt="Average" className="image" />
            <div className="text-content">
                <h3>{average()}</h3>
                <p>Average Heart Rate</p>
            </div>
        </div>
    );
}

export function CountCard({ pulseCount }) {
    const count = () => {
        if (typeof pulseCount === 'number') {
            return pulseCount;
        } else {
            return 'N/A';
        }
    }

    return (
        <div className="main">
            <img src="https://cdn.discordapp.com/attachments/388984743527448576/1126290990681043035/heart.png" alt="Heart" className="image" />
            <div className="text-content">
                <h3>{count()}</h3>
                <p>Number of Entries</p>
            </div>
        </div>
    );
}

export function UpdatedCard({ updated }) {
    const date = () => {
        const dateObj = new Date(updated);

        if (dateObj.toLocaleDateString() === "Invalid Date")
            return "N/A"

        return dateObj.toLocaleDateString()
    }

    return (
        <div className="main">
            <img src="https://cdn.discordapp.com/attachments/388984743527448576/1126307141561618442/date.png" alt="Heart" className="image" />
            <div className="text-content">
                <h3>{date()}</h3>
                <p>Most Recent Entry</p>
            </div>
        </div>
    );
}
