import React from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import './DatePicker.css'

const CustomDatePicker = ({ startDate, endDate, onDateChange }) => {
    const handleDateChange = (name, value) => {
        onDateChange(name, value);
    };

    return (
        <div className="date-picker">
            <DatePicker
                selected={startDate}
                startDate={startDate}
                endDate={endDate}
                onChange={(date) => handleDateChange('start', date)}
                selectsStart
                dateFormat="yyyy-MM-dd"
                placeholderText="Start Date"
            />
            <DatePicker
                selected={endDate}
                startDate={startDate}
                endDate={endDate}
                onChange={(date) => handleDateChange('end', date)}
                selectsEnd
                dateFormat="yyyy-MM-dd"
                placeholderText="End Date"
                minDate={startDate}
            />
        </div>
    );
};

export default CustomDatePicker