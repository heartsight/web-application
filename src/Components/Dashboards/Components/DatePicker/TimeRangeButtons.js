import {subMonths} from "date-fns";
import React from "react";
import './TimeRangeButtons.css'

const TimeRangeButtons = ({ setDateRange }) => {
    const ranges = [1, 2, 3, 6, 12];
    return (
        <div className="time-range-buttons">
            {ranges.map(range => (
                <button key={range} onClick={() => setDateRange({ start: subMonths(new Date(), range), end: new Date() })}>
                    {`${range}m`}
                </button>
            ))}
        </div>
    );
};
export default TimeRangeButtons