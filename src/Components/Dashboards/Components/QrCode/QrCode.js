import React, { useState, useRef } from 'react';
import './QrCode.css';
import QRCode from 'qrcode.react';
import {emailQr} from "../../../../Services/FileService";

const QRCodeModal = ({ isOpen, onRequestClose, userId }) => {
    const [email, setEmail] = useState('');
    const qrCodeRef = useRef(null); // create a reference

    const handleEmailChange = (event) => {
        setEmail(event.target.value);
    }

    const handleEmailSubmit = async () => {
        const file = await getQRCodeBlob();

        await emailQr(file, email);
    }

    const getQRCodeBlob = async () => {
        // Use the ref to access the canvas element.
        const canvas = qrCodeRef.current.querySelector('canvas');

        return new Promise((resolve, reject) => {
            canvas.toBlob((blob) => {
                if (blob) {
                    resolve(blob);
                } else {
                    reject(new Error('Canvas to Blob conversion failed'));
                }
            }, 'image/png');
        });
    }

    if (!isOpen) {
        return null;
    }

    return (
        <div className="main-qr-container">
            <div className="qr-container">
                <div className="manage-close-container">
                    <span className="manage-close-button" onClick={onRequestClose}>&times;</span>
                </div>
                <h2 className="qr-header">Personal QR Code</h2>
                <div className="qr-code" ref={qrCodeRef}>
                    <QRCode value={userId} />
                </div>
                <div className="input-container">
                    <div className="email-password">
                        <h2>Email</h2>
                        <input
                            type="email"
                            value={email}
                            onChange={handleEmailChange}
                            placeholder="Enter your email"
                        />
                        <button
                            onClick={handleEmailSubmit}
                            className="send-button"
                        >
                            Send QR Code
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default QRCodeModal;
