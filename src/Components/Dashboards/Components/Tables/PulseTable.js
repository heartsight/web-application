import React from 'react';
import "./PulseTable.css"

function PulseTable({ pulseData }) {
    return (
        <div className="data-container">
            <div className="header">
                Heart Rate Data
            </div>

            <div className="scroll-container">
                <table>
                    <thead>
                    <tr>
                        <th>Point</th>
                        <th>Date/Time</th>
                        <th>Pulse</th>
                        <th>Anomaly</th>
                        <th>Device ID</th>
                        <th>Device Type</th>
                    </tr>
                    </thead>
                    <tbody>
                    {pulseData.map((pulse, index) => (
                        <tr key={index}>
                            <td>{index + 1}</td>
                            <td>{new Date(pulse.timestamp).toLocaleString('en-US', {
                                weekday: 'long',
                                year: 'numeric',
                                month: 'short',
                                day: 'numeric',
                                hour: '2-digit',
                                minute: '2-digit',
                                hour12: true,
                            })}</td>
                            <td>{pulse.pulseValue}</td>
                            <td>{pulse.anomaly ? 'Yes' : 'No'}</td>
                            <td>{pulse.deviceId}</td>
                            <td>{pulse.deviceType}</td>
                        </tr>
                    ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
}

export default PulseTable;
