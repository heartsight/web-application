import React, { useState } from 'react';
import './SharePulse.css';
import {sharePulse} from "../../../../Services/FileService";

const EmailModal = ({ isOpen, onRequestClose, userId }) => {
    const [email, setEmail] = useState('');
    const [emailSent, setEmailSent] = useState(false); // New state to track if email was sent

    const handleEmailChange = (event) => {
        setEmail(event.target.value);
    }

    const handleEmailSubmit = async () => {

        await sharePulse(userId, email);

        setEmailSent(true); // Set email as sent

        // Reset email sent status after 5 seconds
        setTimeout(() => {
            setEmailSent(false);
        }, 3000);
    }

    if (!isOpen) {
        return null;
    }

    return (
        <div className="main-email-container">
            <div className="email-container">
                <div className="manage-close-container">
                    <span className="manage-close-button" onClick={onRequestClose}>&times;</span>
                </div>
                <h2 className="email-header">Share Pulse Data</h2>
                <div className="input-container">
                    <div className="email-field">
                        <h2>Email</h2>
                        <input
                            type="email"
                            value={email}
                            onChange={handleEmailChange}
                            placeholder="Enter your email"
                        />
                        {emailSent ? (
                            <div className="email-sent-message">Email sent!</div>
                        ) : (
                            <button
                                onClick={handleEmailSubmit}
                                className="send-button"
                            >
                                Send
                            </button>
                        )}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default EmailModal;
