import React, { useState } from 'react';
import './HouseholdListModal.css';
import {createHousehold} from "../../../../Services/HouseholdService";

const HouseholdListModal = ({ isOpen, onRequestClose, households, userId, onLeaveHousehold, onUserLeftHousehold, onCreateHousehold }) => {
    const [newHouseholdName, setNewHouseholdName] = useState('');

    const handleLeaveHousehold = async (household) => {
        if (userId) {
            await onLeaveHousehold(userId, household.householdId, household.role);
            if (onUserLeftHousehold) {
                onUserLeftHousehold();
            }
        } else {
            console.error('No user is currently signed in');
        }
    };

    const handleCreateHousehold = async () => {
        if (newHouseholdName !== '') {
            console.log("Creating household with name:", newHouseholdName);
            await createHousehold(newHouseholdName, userId);

            setNewHouseholdName('');
            if (onCreateHousehold) { // Call the passed-in function to re-fetch
                await onCreateHousehold();
            }
        } else {
            console.error('Household name cannot be empty');
        }
    };

    if (!isOpen) {
        return null;
    }

    return (
        <div className="household-list-modal">
            <div className="household-list-modal-content">
                <div className="household-list-close-container">
                    <span className="manage-close-button" onClick={onRequestClose}>&times;</span>
                </div>

                <h3>Household List</h3>

                <div className="household-list-container">
                    <div className="household-list">
                        {households && households.map((household, index) => (
                            <div className="household-item" key={household.householdId}>
                                <span>{household.householdName}</span>
                                <div className="icon-container">
                                    <button className="leave-button" onClick={(e) => {
                                        e.stopPropagation();
                                        handleLeaveHousehold(household);
                                    }}>
                                        Leave
                                    </button>
                                </div>
                            </div>
                        ))}
                    </div>
                </div>

                <div className="separator"></div>

                <h3>Create Household</h3>

                <div className="create-household">
                    <input
                        type="text"
                        placeholder="Enter new household name"
                        value={newHouseholdName}
                        onChange={e => setNewHouseholdName(e.target.value)}
                    />
                    <button className="create-button" onClick={handleCreateHousehold}>Create</button>
                </div>

            </div>
        </div>
    );
};

export default HouseholdListModal;
