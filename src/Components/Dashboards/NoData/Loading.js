import "./Nodata.css"
const Loading = () => {
    return (
        <div className="loading">
            <div className="message">
                Loading...
            </div>
        </div>
    );
};

export default Loading;
