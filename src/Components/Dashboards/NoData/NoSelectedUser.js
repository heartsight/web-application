import "./Nodata.css"
const NoSelectedUser = () => {
    return (
        <div className="no-selected-user">
            <img className="image" src="https://cdn.discordapp.com/attachments/388984743527448576/1134999856121913364/Untitled-2.png" alt="No Data" />
            <div className="message">
                Please select a household and user to see data.
            </div>
        </div>
    );
};

export default NoSelectedUser;
