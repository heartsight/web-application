import "./Nodata.css"
const NoUserData = () => {
    return (
        <div className="no-data">
            <img className="image" src="https://cdn.discordapp.com/attachments/388984743527448576/1134999856121913364/Untitled-2.png" alt="No Data" />
            <div className="message">
                No user data available.
            </div>
        </div>
    );
};

export default NoUserData;

