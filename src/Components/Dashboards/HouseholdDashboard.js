import React, {useCallback, useEffect, useState} from 'react';
import { subMonths } from 'date-fns';
import { getHouseholdData } from '../../Services/HouseholdService';
import {getRoles, getUserData} from '../../Services/UserService';
import { AnomalyCard, CountCard, UpdatedCard, AverageCard } from './Components/SummaryCard/SummaryCard';
import LineChart from '../Charts/LineChart';
import PulseTable from "./Components/Tables/PulseTable";
import CustomDatePicker from "./Components/DatePicker/DatePicker"
import Members from './Components/HouseholdMenu/Lists/Members';
import HouseholdSelect from "./Components/HouseholdMenu/HouseholdSelect";
import TimeRangeButtons from "./Components/DatePicker/TimeRangeButtons";
import NoUserData from "./NoData/NoUserData"
import NoSelectedUser from "./NoData/NoSelectedUser"
import Loading from "./NoData/Loading"
import Manage from "./Components/HouseholdMenu/Manage/Manage"
import "./Dashboard.css"
import {auth} from "../../Configuration/Firebase";
import {FaDownload, FaShareAlt} from "react-icons/fa";
import {downloadPulse} from "../../Services/FileService";
import SharePulse from "./Components/Share/SharePulse";

const HouseholdDashboard = ({ households}) => {
    const [isEmailModalOpen, setEmailModalOpen] = useState(false);
    const [selectedUser, setSelectedUser] = useState(null);
    const [selectedHousehold, setSelectedHousehold] = useState(null);
    const [users, setUsers] = useState([]);
    const [userData, setUserData] = useState(null);
    const [pulseData, setPulseData] = useState([]);
    const [dateRange, setDateRange] = useState({ start: subMonths(new Date(), 1), end: new Date() });
    const [loading, setLoading] = useState(false);
    const [needRender, setNeedRender] = useState(false);
    const [userRoleLevel, setUserRoleLevel] = useState(0);

    const getRoleLevel = useCallback((userId, roles) => {
        const roleLevels = [
            { level: 5, role: "ownerOf" },
            { level: 4, role: "adminOf" },
            { level: 3, role: "moderatorOf" },
            { level: 2, role: "viewerOf" },
            { level: 1, role: "memberOf" }
        ];

        for (let i = 0; i < roleLevels.length; i++) {
            const { level, role } = roleLevels[i];
            if (roles && roles[role] && roles[role].some(e => e.userId === userId && e.householdId === selectedHousehold)) {
                return level;
            }
        }
        return 0;
    }, [selectedHousehold]);


    const handleSelectHousehold = async (e) => {
        const householdId = e.target.value;
        const data = await getHouseholdData(householdId);
        setSelectedHousehold(householdId);
        setSelectedUser(null);
        setUsers(data);
    };

    const handleSelectUser = async (userId) => {
        setSelectedUser(userId);
    };

    const handleDownload = async () => {
        await downloadPulse(userData.userId)
    };

    const handleShare = () => {
        setEmailModalOpen(true);
    };

    const handleCloseModal = () => {
        setEmailModalOpen(false);
    };

    const getHousehold = useCallback(async (householdId) => {
        const data = await getHouseholdData(householdId);
        setSelectedUser(null);
        setUsers(data);
    }, []);

    useEffect(() => {
        const currentUser = auth.currentUser.uid;
        if (currentUser) {
            const fetchRoles = async () => {
                const roles = await getRoles(currentUser);
                console.log("roles:", roles);
                const roleLevel = getRoleLevel(currentUser, roles);
                console.log("roleLevel:", roleLevel);
                setUserRoleLevel(roleLevel);
            }
            fetchRoles();
        }
    }, [selectedUser, selectedHousehold, getRoleLevel]);

    useEffect(() => {
        const fetchUserData = async () => {
            if (selectedUser) {
                setLoading(true);
                const data = await getUserData(selectedUser, dateRange.start, dateRange.end);
                if (data && data.pulse) {
                    setPulseData(data.pulse);
                    delete data.pulse;
                }
                setUserData(data);
                setLoading(false);
            }
        };
        fetchUserData();
    }, [dateRange.end, dateRange.start, selectedUser]);

    useEffect(() => {
        if (selectedHousehold !== null) {
            getHousehold(selectedHousehold);
        }
    }, [selectedHousehold, getHousehold]);

    useEffect(() => {
        if (needRender) {
            getHousehold(selectedHousehold);
            setNeedRender(false);
        }
    }, [needRender, selectedHousehold, getHousehold]);

    return (
        <div className="Dash">
            <div className="Household-main">
                <div className="Household-menu">
                    <div className="household-select-container">
                        <HouseholdSelect households={households} handleSelectHousehold={handleSelectHousehold} selectedHousehold={selectedHousehold} />
                    </div>
                    <div className="no-household-selected-container">
                        {!selectedHousehold ? (
                            <div className="no-household-selected">
                                <div className="message">
                                    No Household Selected.
                                </div>
                            </div>
                        ) : userRoleLevel >= 2 ? (
                            <div className="roles-container">
                                <Members
                                    users={users}
                                    onSelect={handleSelectUser}
                                />
                            </div>
                        ) : (
                            <></>
                        )}
                    </div>
                    {selectedHousehold && userRoleLevel >= 3 ? (
                        <div className="manage-container">
                            <Manage
                                users={users}
                                onSelect={handleSelectUser}
                                householdId={selectedHousehold}
                                setNeedRender={setNeedRender}
                                userRoleLevel={userRoleLevel}
                            />
                        </div>
                    ) : (
                        <></>
                    )}
                </div>
                <div className="User-data">
                    {loading ? (
                        <Loading/>
                    ) : !selectedHousehold || !selectedUser ? (
                        <NoSelectedUser/>
                    ) : pulseData.length > 0 ? (
                        <>
                            <div className="Dash-chart">
                                <div className="chart-header">
                                    <h2>BPM Trends</h2>
                                    <div className="chart-controls">
                                        <button onClick={handleDownload}><FaDownload /></button>
                                        <button onClick={handleShare}><FaShareAlt /></button>
                                    </div>
                                </div>
                                <div className="time-range-container">
                                    <TimeRangeButtons setDateRange={setDateRange} />
                                    <div className="date-picker">
                                        <CustomDatePicker
                                            startDate={dateRange.start}
                                            endDate={dateRange.end}
                                            onDateChange={(name, value) => setDateRange({ ...dateRange, [name]: value })}
                                        />
                                    </div>
                                </div>
                                <LineChart pulseData={pulseData} />
                            </div>
                            <div className="Dash-summary">
                                <div className="summary">
                                    <CountCard pulseCount={userData ? userData.pulseCount : 0} />
                                </div>
                                <div className="summary">
                                    <AverageCard pulseAvg={userData ? userData.pulseAverage : 0} />
                                </div>
                                <div className="summary">
                                    <UpdatedCard updated={userData ? userData.updated : {}} />
                                </div>
                                <div className="summary">
                                    <AnomalyCard pulseData={pulseData ? pulseData : {}} />
                                </div>
                            </div>
                            <div className="Dash-data">
                                <PulseTable pulseData={pulseData} />
                            </div>
                        </>
                    ) : (
                        <NoUserData/>
                    )}
                </div>
            </div>
            <SharePulse
                isOpen={isEmailModalOpen}
                onRequestClose={handleCloseModal}
                userId={selectedUser}
            />
        </div>
    );
}

export default HouseholdDashboard;