import "../Notifications.css"
import { handleInvite } from "../../../Services/NotificationService";
import {auth} from "../../../Configuration/Firebase";

export default function InviteNotification({notification, refreshNotifications}) {
    const handleButtonClick = async (status) => {
        const userId = auth.currentUser.uid;
        const { householdId, id: inviteId, role } = notification;
        const response = await handleInvite(userId, householdId, inviteId, status, role);

        if (response.success) {
            refreshNotifications();
        } else {
            console.error('Failed to handle invite:', response.error);
        }
    };

    const computeTimeDiff = (dateStr) => {
        const notificationDate = new Date(dateStr);
        const currentDate = new Date();
        const diffInMs = currentDate.getTime() - notificationDate.getTime();
        const diffInHr = diffInMs / (1000 * 60 * 60);
        const diffInDays = diffInMs / (1000 * 60 * 60 * 24);

        if (diffInHr < 24) {
            return `${Math.round(diffInHr)}hr ago`;
        } else {
            return `${Math.round(diffInDays)}d ago`;
        }
    };

    return (
        <div className="notification-main">
            <div className="notification-message">
                <img className="notification-image" src="https://cdn.discordapp.com/attachments/388984743527448576/1134999856121913364/Untitled-2.png" alt="Icon" />
                <div className="notification-text">
                    <p>Invite from {notification.recruiterName}</p>
                    <p>{notification.householdName} [{notification.role}]</p>
                    <p>{computeTimeDiff(notification.created)}</p>
                </div>
            </div>
            <div className="notification-buttons">
                <button className="notification-accept-button" onClick={() => handleButtonClick('accepted')}>Accept</button>
                <button className="notification-decline-button" onClick={() => handleButtonClick('declined')}>Decline</button>
            </div>
        </div>
    );
}
