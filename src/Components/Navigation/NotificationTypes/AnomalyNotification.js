import "../Notifications.css"
import { handleAnomaly } from "../../../Services/NotificationService";
import {auth} from "../../../Configuration/Firebase";

export default function AnomalyNotification({ notification, refreshNotifications }) {
    const closeNotification = async () => {
        const userId = auth.currentUser.uid;
        const anomalyId = notification.id;

        const { success, error } = await handleAnomaly(userId, anomalyId);
        if (success) {
            refreshNotifications();
        } else {
            console.error(`Failed to handle anomaly: ${error}`);
        }
    };

    const computeTimeDiff = (dateStr) => {
        const notificationDate = new Date(dateStr);
        const currentDate = new Date();
        const diffInMs = currentDate.getTime() - notificationDate.getTime();
        const diffInHr = diffInMs / (1000 * 60 * 60);
        const diffInDays = diffInMs / (1000 * 60 * 60 * 24);

        if (diffInHr < 24) {
            return `${Math.round(diffInHr)}hr ago`;
        } else {
            return `${Math.round(diffInDays)}d ago`;
        }
    };

    return (
        <div className="notification-main">
            <div className="notification-message">
                <img className="notification-image" src="https://cdn.discordapp.com/attachments/388984743527448576/1134999856121913364/Untitled-2.png" alt="Icon" />
                <div className="notification-text">
                    <p>Anomaly Detected! ({notification.pulseValue})</p>
                    <p>{computeTimeDiff(notification.created)}</p>
                </div>
            </div>
            <div className="notification-buttons">
                <button className="notification-close-button" onClick={closeNotification}>&times;</button>
            </div>
        </div>
    );
}
