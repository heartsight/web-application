import "../Notifications.css"
import {useEffect, useState} from "react";
import {getUserEmail} from "../../../Services/UserService";

export default function InviteNotificationHousehold({notification, addInviteRoleChange, addInviteChange, isClosed, handleClose, userRoleLevel}) {
    const handleInviteRoleChange = (userId, newRole) => {
        let user = {
            userId: userId,
            inviteId: notification.id,
            role: newRole
        }
        addInviteRoleChange(user);
    };

    const handleRemoveInvitesChange = (userId) => {
        let user = {
            userId: userId,
            inviteId: notification.id
        }
        addInviteChange(user);
    }

    const computeTimeDiff = (dateStr) => {
        const notificationDate = new Date(dateStr);
        const currentDate = new Date();
        const diffInMs = currentDate.getTime() - notificationDate.getTime();
        const diffInHr = diffInMs / (1000 * 60 * 60);
        const diffInDays = diffInMs / (1000 * 60 * 60 * 24);

        if (diffInHr < 24) {
            return `${Math.round(diffInHr)}hr ago`;
        } else {
            return `${Math.round(diffInDays)}d ago`;
        }
    };

    const useEmail = (userId) => {
        const [email, setEmail] = useState('');
        useEffect(() => {
            const fetchEmail = async () => {
                const email = await getUserEmail(userId);
                setEmail(email);
            };
            fetchEmail();
        }, [userId]);
        return email;
    };

    const email = useEmail(notification.userId);

    const roleToLevel = (role) => {
        switch (role) {
            case "Viewer":
                return 1;
            case "Member":
                return 2;
            case "Moderator":
                return 3;
            case "Admin":
                return 4;
            case "Owner":
                return 5;
            default:
                return 0;
        }
    };

    return (
        <div className={`pending-invite-user-item ${isClosed ? 'removed' : ''}`}>
            <div className="pending-invite-email">
                <span>{email}</span>
            </div>
            <div className="pending-invite-date">
                <p>{computeTimeDiff(notification.created)}</p>
            </div>
            {userRoleLevel > roleToLevel(notification.role) ? (
                <div className="user-dropdown">
                    <select defaultValue={notification.role} onChange={(e) => handleInviteRoleChange(notification.userId, e.target.value)}>
                        {userRoleLevel > roleToLevel("Viewer") && <option value="Viewer">Viewer</option>}
                        {userRoleLevel > roleToLevel("Member") && <option value="Member">Member</option>}
                        {userRoleLevel > roleToLevel("Moderator") && <option value="Moderator">Moderator</option>}
                        {userRoleLevel > roleToLevel("Admin") && <option value="Admin">Admin</option>}
                        {userRoleLevel > roleToLevel("Owner") && <option value="Owner">Owner</option>}
                    </select>
                </div>
            ) : (
                <div className="user-role-display">{notification.role}</div>
            )}
            {userRoleLevel > roleToLevel(notification.role) && (
                <div className="user-close-button-container">
                    <button
                        className="user-close-button"
                        onClick={() => {
                            handleRemoveInvitesChange(notification.userId);
                            handleClose(notification.id);
                        }}
                    >
                        &times;
                    </button>
                </div>
            )}
        </div>
    );
}
