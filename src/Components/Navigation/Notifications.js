import React, {useCallback, useEffect, useState} from 'react';
import './Notifications.css';
import { retrieveNotifications } from "../../Services/NotificationService";
import { auth } from "../../Configuration/Firebase";
import AnomalyNotification from "./NotificationTypes/AnomalyNotification";
import InviteNotification from "./NotificationTypes/InviteNotification";

export default function Notifications({ isPopupOpen, setPopperElement, styles, attributes }) {
    const [, setNotifications] = useState({
        anomalies: [],
        invites: []
    });

    const [anomalyNotifications, setAnomalyNotifications] = useState([]);
    const [inviteNotifications, setInviteNotifications] = useState([]);

    const fetchNotifications = useCallback(async () => {
        const { success, data, error } = await retrieveNotifications(auth.currentUser.uid);

        if (success) {
            setNotifications(data);

            const anomalies = [];
            const invites = [];

            data.anomalies.forEach(notification => {
                anomalies.push(<AnomalyNotification key={notification.id} notification={notification} refreshNotifications={fetchNotifications} />);
            });

            data.invites.forEach(notification => {
                invites.push(<InviteNotification key={notification.id} notification={notification} refreshNotifications={fetchNotifications} />);
            });

            setAnomalyNotifications(anomalies);
            setInviteNotifications(invites);
        } else {
            console.error('Failed to retrieve notifications:', error);
        }
    }, []);

    useEffect(() => {
        fetchNotifications();
    }, [fetchNotifications]);

    return (
        <>
            {isPopupOpen && (
                <div
                    ref={setPopperElement}
                    style={styles.popper}
                    {...attributes.popper}
                    className="notification-popup"
                >
                    <div className="notification-title">Notifications</div>
                    {(anomalyNotifications.length === 0 && inviteNotifications.length === 0) ?
                        <div className="no-notifications">No Notifications</div> :
                        (
                            <>
                                <div className="notification-category">
                                    <div className="notification-subtitle">Invites</div>
                                    {inviteNotifications}
                                </div>
                                <div className="notification-category">
                                    <div className="notification-subtitle">Anomalies</div>
                                    {anomalyNotifications}
                                </div>
                            </>
                        )}
                </div>
            )}
        </>
    );
}
