import React, {useEffect, useState, useRef} from 'react';
import { Nav } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { logout } from '../../Services/AuthService';
import {useNavigate, useLocation, Link} from "react-router-dom";
import "./Navigation.css"
import Logo from './Logo/hv_logo.png'
import Notifications from "./Notifications"
import {usePopper} from "react-popper";
import {FaBell} from "react-icons/fa";

export default function Navigation() {
    const navigate = useNavigate();
    const location = useLocation();
    const isMainPage = location.pathname === '/';
    const isLoggedIn = JSON.parse(localStorage.getItem("isLoggedIn"));

    const [isPopupOpen, setIsPopupOpen] = useState(false);
    const [referenceElement, setReferenceElement] = useState(null);
    const [popperElement, setPopperElement] = useState(null);
    const { styles, attributes } = usePopper(referenceElement, popperElement);
    const notificationIconRef = useRef();

    const handleLogout = async () => {
        try {
            await logout();
            navigate("/");
        } catch (error) {
            console.error('Error signing out:', error);
        }
    };

    const [selectedLink, setSelectedLink] = useState(null);  // Initialize selectedLink to null

    const handleNavigation = (sectionId, route) => {
        setSelectedLink(sectionId || route);  // Set selectedLink immediately when user clicks
        if (isMainPage && sectionId) {
            document.getElementById(sectionId).scrollIntoView({ behavior: 'smooth' });
        } else if (route) {
            navigate(route);
        } else {
            navigate("/");
            localStorage.setItem("navigateToSection", sectionId);
        }
    }

    useEffect(() => {
        const sectionId = localStorage.getItem("navigateToSection");
        if (isMainPage && sectionId) {
            document.getElementById(sectionId).scrollIntoView({ behavior: 'smooth' });
            localStorage.removeItem("navigateToSection");
        }
    }, [isMainPage]);

    useEffect(() => {
        function handleClickOutside(event) {
            if (popperElement && !popperElement.contains(event.target) && !notificationIconRef.current.contains(event.target)) {
                setIsPopupOpen(false);
            }
        }

        document.addEventListener('mousedown', handleClickOutside);

        return () => {
            document.removeEventListener('mousedown', handleClickOutside);
        };
    }, [popperElement]);

    return (
        <>
            <Nav className="nav">
                <div className="nav-left">
                    <LinkContainer to="/">
                        <div className="nav-left">
                            <img src={Logo} alt="Logo" className="logo"/>
                        </div>
                    </LinkContainer>
                </div>

                <div className="nav-middle">
                    <Nav.Link className={`nav-link ${selectedLink === 'home-section' ? 'active' : ''}`} onClick={() => handleNavigation('home-section')}>Home</Nav.Link>
                    <Nav.Link className={`nav-link ${selectedLink === 'about-section' ? 'active' : ''}`} onClick={() => handleNavigation('about-section')}>About Us</Nav.Link>
                    <Nav.Link className={`nav-link ${selectedLink === 'contact-section' ? 'active' : ''}`} onClick={() => handleNavigation( 'contact-section')}>Contact</Nav.Link>
                </div>

                <div className="nav-right">
                    {isLoggedIn ? (
                        <>
                            <div
                                ref={el => { setReferenceElement(el); notificationIconRef.current = el; }}
                                onClick={() => setIsPopupOpen(true)}
                                tabIndex="0"
                                className={`nav-link ${selectedLink === '/notifications' ? 'active' : ''}`}
                            >
                                <FaBell />
                            </div>
                            <Notifications
                                isPopupOpen={isPopupOpen}
                                setPopperElement={setPopperElement}
                                styles={styles}
                                attributes={attributes}
                            />
                            <Nav.Link className={`nav-link ${selectedLink === '/dashboard' ? 'active' : ''}`} onClick={() => handleNavigation(null, '/dashboard')}>Dashboard</Nav.Link>
                            <Nav.Link onClick={handleLogout} className="nav-link">Logout</Nav.Link>
                        </>
                    ) : (
                        <>
                            <Nav.Link className={`nav-link ${selectedLink === '/sign-in' ? 'active' : ''}`} onClick={() => handleNavigation(null, '/sign-in')}>Sign In</Nav.Link>
                            <button className="get-started-btn" onClick={() => handleNavigation(null, '/create-account')}>
                                <Link to="/create-account">
                                    <span className="get-started-btn-text">Get Started</span>
                                </Link>
                            </button>
                        </>
                    )}
                </div>
            </Nav>
            <div className="nav-spacer"></div>
        </>
    );
}
