import React from "react";
import Home from '../Home/Home';
import AboutUs from "../AboutUs/AboutUs";
import Contact from "../Contact/Contact";

function MainPage() {
    return (
        <div>
            <div id="home-section">
                <Home />
            </div>

            <div id="about-section">
                <AboutUs />
            </div>

            <div id="contact-section">
                <Contact />
            </div>
        </div>
    );
}

export default MainPage;
