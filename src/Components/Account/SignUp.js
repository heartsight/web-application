import { useState } from "react";
import {Link, useNavigate} from 'react-router-dom';
import { createAccount } from "../../Services/UserService"
import {authenticate, create} from "../../Services/AuthService"
import "./SignUp.css"

function SignUp() {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [gender, setGender] = useState("");
    const [dateOfBirth, setDateOfBirth] = useState("");

    const navigate = useNavigate();

    const register = async () => {
        try {
            // Create account with firebase and use unique id to store additional metadata in firestore
            let userCredential = await create(email, password);
            let userId = userCredential.user.uid
            const accountCreated = await createAccount(userId, email, firstName, lastName, dateOfBirth, gender, 25);

            if (accountCreated) {
                await authenticate(email, password)

                // Navigate to dashboard
                navigate('/dashboard');
            } else {
                console.log("Failed to create account");
            }
        } catch (error) {
            console.log(error.message);
        }
    };

    return (
        <div className="main-signin-container">
            <div className="account-creation">
                <div className="input-container">
                    <div className="create-account-container">
                        <h1>Create new account.</h1>
                        <div className="signin-container">
                            <p>Already a member? <Link to="/sign-in">Sign In</Link></p>
                        </div>
                    </div>

                    <div className="input-container">
                        <div className="input-pair">
                            <div className="inputs">
                                <h2>First Name </h2>
                                <input
                                    type="text"
                                    placeholder="First Name"
                                    onChange={(e) => setFirstName(e.target.value)}
                                />
                            </div>

                            <div className="inputs">
                                <h2>Last Name </h2>
                                <input
                                    type="text"
                                    placeholder="Last Name"
                                    onChange={(e) => setLastName(e.target.value)}
                                />
                            </div>
                        </div>

                        <div className="input-pair">
                            <div className="inputs">
                                <h2>Gender </h2>
                                <select onChange={(e) => setGender(e.target.value)}>
                                    <option value="">Select Gender</option>
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                    <option value="other">Other</option>
                                </select>
                            </div>

                            <div className="inputs">
                                <h2>Date of Birth </h2>
                                <input
                                    type="date"
                                    placeholder="Date of Birth"
                                    onChange={(e) => setDateOfBirth(e.target.value)}
                                />
                            </div>
                        </div>

                        <div className="inputs">
                            <label>Email *</label>
                            <input
                                type="email"
                                placeholder="Enter email"
                                onChange={(e) => setEmail(e.target.value)}
                            />
                        </div>

                        <div className="inputs">
                            <h2>Password *</h2>
                            <input
                                type="password"
                                placeholder="Enter password"
                                onChange={(e) => setPassword(e.target.value)}
                            />
                        </div>
                        <div className="signup-warning">
                            <p>* Required field</p>
                        </div>
                        <button className="signup-button" onClick={register}>Create Account</button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default SignUp;
