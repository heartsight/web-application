import { useState } from "react";
import {Link, useNavigate} from 'react-router-dom';
import { authenticate } from "../../Services/AuthService";
import "./SignIn.css"

function SignIn() {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const navigate = useNavigate();

    const login = async () => {
        try {
            // Authenticate user
            await authenticate(email, password)

            // Navigate to dashboard
            navigate('/dashboard');
        } catch (error) {
            // Sign-in failed, display an error message
            console.log(error.message);
        }
    };

    return (
        <div className="main-login-container">
            <div className="login-container">
                <div className="input-container">
                    <div className="welcome-container">
                        <h1>Welcome back!</h1>
                        <h3>Please enter your details.</h3>
                    </div>

                    <div className="input-container">
                        <div className="email-password">
                            <h2>Email</h2>
                            <input
                                type={"email"}
                                placeholder="Enter email"
                                onChange={(e) => setEmail(e.target.value)}
                            />
                        </div>
                        <div className="email-password">
                            <h2>Password</h2>
                            <input
                                type={"password"}
                                placeholder="Enter password"
                                onChange={(e) => setPassword(e.target.value)}
                            />
                        </div>
                        <button className="signin-button" onClick={login}>Sign in</button>
                    </div>

                    <div className="join-container">
                        <p>New to Heart Vision? <Link to="/create-account">Join Now</Link></p>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default SignIn;
