import "./App.css";
import { BrowserRouter as Router } from "react-router-dom";
import AppRouter from "./AppRouter";
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
    return (
        <Router>
            <div className="App">
                <div className="app-container">
                    <AppRouter />
                </div>
            </div>
        </Router>
    );
}

export default App;