import { createUserWithEmailAndPassword, getAuth, signInWithEmailAndPassword, signOut } from "firebase/auth";
import { app } from "../Configuration/Firebase";

const auth = getAuth(app);

export const create = async (email, password) => {
    try {
        return await createUserWithEmailAndPassword(auth, email, password)
    } catch (error) {
        throw error;
    }
};

export const authenticate = async (email, password) => {
    try {
        await signInWithEmailAndPassword(auth, email, password);
        localStorage.setItem("isLoggedIn", JSON.stringify(true));
    } catch (error) {
        throw error;
    }
};

export const logout = async () => {
    try {
        await signOut(auth);
        localStorage.removeItem("isLoggedIn");
    } catch (error) {
        throw error;
    }
};
