const gradient = {
    colors: [
        "rgb(189,84,115)",
        "rgb(175,89,135)",
        "rgb(152,95,149)",
        "rgb(122,101,169)"
    ],
    position: 0,
};

export const getGradientLine = (context) => {
    const chart = context.chart;
    const { ctx, chartArea } = chart;
    if (!chartArea) {
        return null;
    }
    const gradientLine = ctx.createLinearGradient(
        chartArea.left,
        chartArea.top,
        chartArea.right,
        chartArea.bottom
    );
    gradientLine.addColorStop(gradient.position, gradient.colors[0]);
    gradientLine.addColorStop(.25, gradient.colors[1]);
    gradientLine.addColorStop(.50, gradient.colors[2]);
    gradientLine.addColorStop(.75, gradient.colors[3]);
    return gradientLine;
};

export const getGradientBackground = (context) => {
    const chart = context.chart;
    const { ctx, chartArea } = chart;
    if (!chartArea) {
        return null;
    }
    const gradientBackground = ctx.createLinearGradient(
        0,
        chartArea.top,
        0,
        chartArea.bottom
    );
    gradientBackground.addColorStop(gradient.position, 'rgba(81, 63, 72, .9)');
    gradientBackground.addColorStop(.75, 'rgba(81, 63, 72, .45)');
    return gradientBackground;
};
