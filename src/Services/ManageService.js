export const modifyUserNickname = async (users, householdId) => {
    const requestBody = {
        users: users, // An array of user objects { userId: "", role: "", newNickname: "" }
        householdId: householdId, // String for the household id
    };

    console.log("modifyUserNickname requestBody: ", requestBody);

    const response = await fetch('https://pulse.heartvision.cloud/Manage/modifyUserNickname', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestBody)
    });

    if (!response.ok) {
        console.log(`HTTP error, status = ${response.status}`);
        return { success: false, error: `HTTP error, status = ${response.status}` };
    }

    return { success: true };
};

export const modifyUserRole = async (users, householdId) => {
    const requestBody = {
        users: users, // An array of user objects { userId: "", role: "", newNickname: "" }
        householdId: householdId, // String for the household id
    };

    console.log("modifyUserRole requestBody: ", requestBody); // Log the request body

    const response = await fetch('https://pulse.heartvision.cloud/Manage/modifyUserRole', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestBody)
    });

    if (!response.ok) {
        console.log(`HTTP error, status = ${response.status}`);
        return { success: false, error: `HTTP error, status = ${response.status}` };
    }

    return { success: true };
};

export const modifyNotificationMode = async (users, householdId) => {
    const requestBody = {
        users: users, // An array of user objects { userId: "", role: "", newNickname: "" }
        householdId: householdId, // String for the household id
    };

    console.log("modifyUserRole requestBody: ", requestBody); // Log the request body

    const response = await fetch('https://pulse.heartvision.cloud/Manage/modifyNotificationMode', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestBody)
    });

    if (!response.ok) {
        console.log(`HTTP error, status = ${response.status}`);
        return { success: false, error: `HTTP error, status = ${response.status}` };
    }

    return { success: true };
};

export const modifyPendingInvites = async (users, householdId) => {
    const requestBody = {
        users: users, // An array of user objects { userId: "", role: "", newNickname: "" }
        householdId: householdId, // String for the household id
    };

    console.log("modifyUserRole requestBody: ", requestBody); // Log the request body

    const response = await fetch('https://pulse.heartvision.cloud/Manage/modifyPendingInvites', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestBody)
    });

    if (!response.ok) {
        console.log(`HTTP error, status = ${response.status}`);
        return { success: false, error: `HTTP error, status = ${response.status}` };
    }

    return { success: true };
};

export const removeInvites = async (users, householdId) => {
    const requestBody = {
        users: users, // An array of user objects { userId: "", role: "", newNickname: "" }
        householdId: householdId, // String for the household id
    };

    console.log("removeInvites requestBody: ", requestBody); // Log the request body

    const response = await fetch('https://pulse.heartvision.cloud/Manage/removeInvites', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestBody)
    });

    if (!response.ok) {
        console.log(`HTTP error, status = ${response.status}`);
        return { success: false, error: `HTTP error, status = ${response.status}` };
    }

    return { success: true };
};

export const retrieveNotifications = async (householdId) => {
    const requestBody = {
        householdId: householdId, // String for the household id
    };

    const response = await fetch('https://pulse.heartvision.cloud/Manage/retrieveNotifications', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestBody)
    });

    if (!response.ok) {
        console.log(`HTTP error, status = ${response.status}`);
        return { success: false, error: `HTTP error, status = ${response.status}` };
    }

    const data = await response.json(); // Parse the response body

    return { success: true, invites: data.invites }; // Include invites in the return object
};

export const removeUsers = async (users, householdId) => {
    const requestBody = {
        users: users, // An array of user objects { userId: "", role: "", newNickname: "" }
        householdId: householdId, // String for the household id
    };

    const response = await fetch('https://pulse.heartvision.cloud/Manage/removeUsers', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestBody)
    });

    if (!response.ok) {
        console.log(`HTTP error, status = ${response.status}`);
        return { success: false, error: `HTTP error, status = ${response.status}` };
    }

    return { success: true };
};

export const sendInvite = async (email, nickname, householdId, currentUserId, role) => {
    const requestBody = {
        userEmail: email,
        userNickname: nickname,
        householdId: householdId,
        recruiterId: currentUserId,
        role: role
    };

    console.log(requestBody);

    const response = await fetch('https://pulse.heartvision.cloud/Manage/invite', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestBody)
    });

    if (!response.ok) {
        console.log(`HTTP error, status = ${response.status}`);
        return { success: false, error: `HTTP error, status = ${response.status}` };
    }

    return { success: true };
};
