export const emailQr = async (file, email) => {
    const formData = new FormData();
    formData.append('File', file, 'QRCode.png');
    formData.append('ToEmail', email);

    const response = await fetch('https://pulse.heartvision.cloud/File/sendQrCode', {
        method: 'POST',
        body: formData
    });

    if (!response.ok) {
        console.log(`HTTP error, status = ${response.status}`);
        return false;
    }

    return await response.json();
};

export const downloadPulse = async (userId) => {
    const requestBody = {
        UserId: userId,
    };

    try {
        const response = await fetch('https://pulse.heartvision.cloud/File/downloadPulse', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(requestBody)
        });

        if (!response.ok) {
            console.log(`HTTP error, status = ${response.status}`);
            return false;
        }

        const blob = await response.blob();
        const url = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        a.href = url;

        // If the Content-Disposition header is available, you can extract the filename from it
        const contentDisposition = response.headers.get('Content-Disposition');
        let filename = 'pulse-data.csv';
        if (contentDisposition) {
            const match = contentDisposition.match(/filename="(.+)"/);
            if (match && match[1]) filename = match[1];
        }

        a.download = filename;
        document.body.appendChild(a);
        a.click();
        window.URL.revokeObjectURL(url);
        return true;
    } catch (error) {
        console.error('Download failed:', error);
        return false;
    }
};

export const sharePulse = async (userId, email) => {
    const requestBody = {
        UserId: userId,
        ToEmail: email
    };

    const response = await fetch('https://pulse.heartvision.cloud/File/sendPulse', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestBody)
    });

    if (!response.ok) {
        console.log(`HTTP error, status = ${response.status}`);
        return false;
    }
}
