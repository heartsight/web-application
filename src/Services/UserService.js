export async function getRoles(userId) {
    const requestBody = {
        UserId: userId,
    };

    // Retrieves user info for {userId}
    const response = await fetch('https://pulse.heartvision.cloud/Account/retrieveRoles', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestBody)
    });

    if (!response.ok) {
        console.log(`HTTP error, status = ${response.status}`)
        return false
    }

    return await response.json();
}

export const getUserData = async (userId, start, end) => {
    const requestBody = {
        UserId: userId,
        StartDate: start,
        EndDate: end
    };

    // Retrieves user info for {userId}
    const response = await fetch('https://pulse.heartvision.cloud/Account/retrieve', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestBody)
    });

    if (!response.ok) {
        console.log(`HTTP error, status = ${response.status}`)
        return false
    }

   return await response.json();
};

export const getUserEmail = async (userId) => {
    const requestBody = {
        UserId: userId,
    };

    const response = await fetch('https://pulse.heartvision.cloud/Account/retrieveEmail', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestBody)
    });

    if (!response.ok) {
        console.log(`HTTP error, status = ${response.status}`)
        return null;
    }

    return await response.text();
};

export const createAccount = async (userId, email, firstName, lastName, dateOfBirth, gender, age) => {
    const requestBody = {
        UserId: userId,
        Email: email,
        FirstName: firstName,
        LastName: lastName,
        DateOfBirth: dateOfBirth,
        Gender: gender,
        Age: age
    };

    console.log(JSON.stringify(requestBody))

    const response = await fetch('https://pulse.heartvision.cloud/Account/create', {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestBody)
    });

    if (!response.ok) {
        console.log(`API request failed with status ${response.status}`)
        return false
    }

    // alert("Successfully created an account and sent data to your backend API!");
    return true
}

export const removeUserFromHousehold = async (userId, householdId, role) => {
    const requestBody = {
        UserId: userId,
        HouseholdId: householdId,
        Role: role,
    };

    const response = await fetch('https://pulse.heartvision.cloud/Household/leave', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestBody)
    });

    if (!response.ok) {
        console.log(`HTTP error, status = ${response.status}`);
        return { success: false, error: `HTTP error, status = ${response.status}` };
    }

    return { success: true };
};

export const verifyEmail = async (email) => {
    const requestBody = {
        Email: email
    };

    const response = await fetch('https://pulse.heartvision.cloud/Account/verifyEmail', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestBody)
    });

    if (!response.ok) {
        console.log(`HTTP error, status = ${response.status}`)
        return false
    }

    const responseBody = await response.text();
    console.log("Response: ", responseBody);

    return responseBody;
};


