export const getHouseholdData = async (householdId) => {
    const requestBody = {
        HouseholdId: householdId
    };

    // Retrieves user info for {userId}
    const response = await fetch('https://pulse.heartvision.cloud/Household/retrieve', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestBody)
    });

    if (!response.ok) {
        console.log(`HTTP error, status = ${response.status}`)
        return false
    }

    return await response.json();
};

export const createHousehold = async (householdName, userId) => {
    const requestBody = {
        HouseholdName: householdName,
        HouseholdOwner: userId
    };

    console.log("Request Body:", requestBody);

    // Retrieves user info for {userId}
    const response = await fetch('https://pulse.heartvision.cloud/Household/create', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestBody)
    });

    if (!response.ok) {
        console.log(`HTTP error, status = ${response.status}`)
        return false
    }
};