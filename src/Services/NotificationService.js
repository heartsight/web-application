export const retrieveNotifications = async (userId) => {
    const requestBody = {
        userId: userId
    };

    const response = await fetch('https://pulse.heartvision.cloud/Notification/retrieveNotifications', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestBody)
    });

    if (!response.ok) {
        console.log(`HTTP error, status = ${response.status}`);
        return { success: false, error: `HTTP error, status = ${response.status}` };
    }

    const data = await response.json();
    return { success: true, data };
};

export const handleInvite = async (userId, householdId, inviteId, status, role) => {
    const requestBody = {
        userId: userId,
        householdId: householdId,
        inviteId: inviteId,
        status: status,
        role: role
    };

    const response = await fetch('https://pulse.heartvision.cloud/Notification/handleInvite', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestBody)
    });

    if (!response.ok) {
        console.log(`HTTP error, status = ${response.status}`);
        return { success: false, error: `HTTP error, status = ${response.status}` };
    }

    const data = await response.json();
    return { success: true, data };
};

export const handleAnomaly = async (userId, anomalyId) => {
    const requestBody = {
        userId: userId,
        anomalyId: anomalyId
    };

    const response = await fetch('https://pulse.heartvision.cloud/Notification/handleAnomaly', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestBody)
    });

    if (!response.ok) {
        console.log(`HTTP error, status = ${response.status}`);
        return { success: false, error: `HTTP error, status = ${response.status}` };
    }

    const data = await response.json();
    return { success: true, data };
};
