// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from 'firebase/auth';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyBo-uhR8cHdciQ6bWLEubEIci1EJi7MIow",
    authDomain: "heart-vision.firebaseapp.com",
    projectId: "heart-vision",
    storageBucket: "heart-vision.appspot.com",
    messagingSenderId: "1002389817382",
    appId: "1:1002389817382:web:b9b06dc6ba78fed9433ee7"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// Initialize Firebase Authentication
const auth = getAuth(app);

export { app, auth };
