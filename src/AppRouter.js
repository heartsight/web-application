import { Routes, Route } from "react-router-dom";
import Navigation from "./Components/Navigation/Navigation";
import SignUp from "./Components/Account/SignUp";
import Dashboard from "./Components/Dashboards/Dashboard";
import Reports from "./Components/Reports/Reports";
import Households from "./Components/Households/Households";
import Settings from "./Components/Settings/Settings";
import Help from "./Components/Help/Help";
import Main from "./Components/Main/MainPage"
import "./App.css"
import SignIn from "./Components/Account/SignIn";

function AppRouter() {
    return (
        <div className="main-content">
            <Navigation />
            <div className="app-content">
                <div className="App-header">
                    <Routes>
                        <Route path="/" element={<Main />} />
                        <Route path="/sign-in" element={<SignIn />} />
                        <Route path="/create-account" element={<SignUp />} />

                        <Route path="/dashboard" element={<Dashboard />} />
                        <Route path="/reports" element={<Reports />} />
                        <Route path="/households" element={<Households />} />

                        <Route path="/settings" element={<Settings />} />
                        <Route path="/help" element={<Help />} />
                    </Routes>
                </div>
            </div>
        </div>
    );
}

export default AppRouter;